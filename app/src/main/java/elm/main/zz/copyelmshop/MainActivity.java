package elm.main.zz.copyelmshop;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

public class MainActivity extends AppCompatActivity implements
        StickyListHeadersListView.OnHeaderClickListener,
        StickyListHeadersListView.OnStickyHeaderChangedListener,
        StickyListHeadersListView.OnStickyHeaderOffsetChangedListener,
        View.OnClickListener, GoodsListAdapter.OnShopCartGoodsChangeListener {

    private ListView goodsCateGoryList;
    private StickyListHeadersListView goodsList;
    private Context ctx;
    private RelativeLayout shopCartMain;
    private ImageView image;
    private TextView shopCartNum;
    private TextView totalPrice;
    private TextView howMoneyToDelivery;
    private TextView goTOCheckOut;
    private String testJson = "{\"data\":[{\"goodscatrgory\":{\"goodsitem\":[{\"name\":\"银鸽\"},{\"name\":\"清风\"},{\"name\":\"心相印\"}],\"name\":\"卫生纸\"}},{\"goodscatrgory\":{\"goodsitem\":[{\"name\":\"中华牙膏\"},{\"name\":\"舒克牙膏\"},{\"name\":\"云南白牙膏\"}],\"name\":\"口腔护理\"}},{\"goodscatrgory\":{\"goodsitem\":[{\"name\":\"昆仑山矿泉水\"},{\"name\":\"康师傅冰红茶\"},{\"name\":\"恒大冰泉\"}],\"name\":\"饮料/水\"}},{\"goodscatrgory\":{\"goodsitem\":[{\"name\":\"昆仑山矿泉水\"},{\"name\":\"康师傅冰红茶\"},{\"name\":\"恒大冰泉\"}],\"name\":\"饮料/水\"}}]}";

    //商品类别列表
    private List<GoodsListBean.DataEntity.GoodscatrgoryEntity> goodscatrgoryEntities;
    //商品列表
    private List<GoodsListBean.DataEntity.GoodscatrgoryEntity.GoodsitemEntity> goodsitemEntities;
    private GoodsListAdapter goodsAdapter;
    private GoodsCategoryListAdapter goodsCategoryListAdapter;
    private MyOnGoodsScrollListener myOnGoodsScrollListener;
    //上一个标题的小标
    private int lastTitlePoi;
    //存储含有标题的第一个含有商品类别名称的条目的下表
    private List<Integer> titlePois = new ArrayList<>();


    private void assignViews() {
        goodsCateGoryList = (ListView) findViewById(R.id.goodsCateGoryList);
        goodsList = (StickyListHeadersListView) findViewById(R.id.goodsList);
        shopCartMain = (RelativeLayout) findViewById(R.id.shopCartMain);
        image = (ImageView) findViewById(R.id.image);
        shopCartNum = (TextView) findViewById(R.id.shopCartNum);
        totalPrice = (TextView) findViewById(R.id.totalPrice);
        howMoneyToDelivery = (TextView) findViewById(R.id.howMoneyToDelivery);
        goTOCheckOut = (TextView) findViewById(R.id.goTOCheckOut);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        assignViews();
        ctx = this;
        initListener();
        fillGoodsList();
    }

    /**
     * 初始化监听数据
     */
    private void initListener() {
        goodsList.setOnHeaderClickListener(this);
        goodsList.setOnStickyHeaderChangedListener(this);
        goodsList.setOnStickyHeaderOffsetChangedListener(this);
        goodsList.setDrawingListUnderStickyHeader(true);
        goodsList.setAreHeadersSticky(true);
        shopCartMain.setOnClickListener(this);
        goTOCheckOut.setOnClickListener(this);
        goodsCateGoryList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                goodsList.setSelection(titlePois.get(position));
            }
        });
    }

    /**
     * 填充商品列表
     */
    private void fillGoodsList() {
        goodscatrgoryEntities = new ArrayList<>();
        goodsitemEntities = new ArrayList<>();
        GoodsListBean mainBean = new Gson().fromJson(testJson, GoodsListBean.class);
        int i = 0;
        int j = 0;
        boolean isFirst;
        for (GoodsListBean.DataEntity dataItem :
                mainBean.getData()) {
            goodscatrgoryEntities.add(dataItem.getGoodscatrgory());
            isFirst = true;
            for (GoodsListBean.DataEntity.GoodscatrgoryEntity.GoodsitemEntity goodsitemEntity :
                    dataItem.getGoodscatrgory().getGoodsitem()
                    ) {
                if (isFirst) {
                    titlePois.add(j);
                    isFirst = false;
                }
                j++;
                goodsitemEntity.setCategoryPosition(i);
                goodsitemEntities.add(goodsitemEntity);
            }
            i++;
        }
        goodsAdapter = new GoodsListAdapter(goodsitemEntities, ctx, goodscatrgoryEntities);
        goodsAdapter.setmActivity(this);
        goodsAdapter.setShopCart(shopCartNum);
        goodsList.setAdapter(goodsAdapter);
        goodsAdapter.setOnShopCartGoodsChangeListener(this);
        goodsCategoryListAdapter = new GoodsCategoryListAdapter(goodscatrgoryEntities, ctx);
        goodsCateGoryList.setAdapter(goodsCategoryListAdapter);
        myOnGoodsScrollListener = new MyOnGoodsScrollListener();
        goodsList.setOnScrollListener(myOnGoodsScrollListener);
    }

    @Override
    public void onHeaderClick(StickyListHeadersListView l, View header, int itemPosition, long headerId, boolean currentlySticky) {

    }

    @Override
    public void onStickyHeaderChanged(StickyListHeadersListView l, View header, int itemPosition, long headerId) {

    }

    @Override
    public void onStickyHeaderOffsetChanged(StickyListHeadersListView l, View header, int offset) {

    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onNumChange() {

    }

    /**
     * 处理滑动 是两个ListView联动
     */
    private class MyOnGoodsScrollListener implements AbsListView.OnScrollListener {

        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {

        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            if (!(lastTitlePoi == goodsitemEntities
                    .get(firstVisibleItem)
                    .getCategoryPosition())) {
                lastTitlePoi = goodsitemEntities
                        .get(firstVisibleItem)
                        .getCategoryPosition();
                goodsCategoryListAdapter.setCheckID(goodsitemEntities
                        .get(firstVisibleItem)
                        .getCategoryPosition());
                goodsCategoryListAdapter.notifyDataSetChanged();
            }
        }
    }
}
