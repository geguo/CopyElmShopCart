package elm.main.zz.copyelmshop;

import java.util.List;

/**
 * 说明：封装商品的Bean
 * 作者：陈杰宇
 * 时间： 2016/2/20 18:11
 * 版本：V1.0
 * 修改历史：
 */
public class GoodsListBean {


    /**
     * goodscatrgory : {"goodsitem":[{"name":"银鸽"},{"name":"清风"},{"name":"心相印"}],"name":"卫生纸"}
     */

    private List<DataEntity> data;

    public void setData(List<DataEntity> data) {
        this.data = data;
    }

    public List<DataEntity> getData() {
        return data;
    }

    public static class DataEntity {
        /**
         * goodsitem : [{"name":"银鸽"},{"name":"清风"},{"name":"心相印"}]
         * name : 卫生纸
         */

        private GoodscatrgoryEntity goodscatrgory;

        public void setGoodscatrgory(GoodscatrgoryEntity goodscatrgory) {
            this.goodscatrgory = goodscatrgory;
        }

        public GoodscatrgoryEntity getGoodscatrgory() {
            return goodscatrgory;
        }

        public static class GoodscatrgoryEntity {
            private String name;
            /**
             * name : 银鸽
             */

            private List<GoodsitemEntity> goodsitem;

            public void setName(String name) {
                this.name = name;
            }

            public void setGoodsitem(List<GoodsitemEntity> goodsitem) {
                this.goodsitem = goodsitem;
            }

            public String getName() {
                return name;
            }

            public List<GoodsitemEntity> getGoodsitem() {
                return goodsitem;
            }

            public static class GoodsitemEntity {
                private String name;

                public int getCategoryPosition() {
                    return categoryPosition;
                }

                public void setCategoryPosition(int categoryPosition) {
                    this.categoryPosition = categoryPosition;
                }

                private int categoryPosition;

                public void setName(String name) {
                    this.name = name;
                }

                public String getName() {
                    return name;
                }
            }
        }
    }
}
